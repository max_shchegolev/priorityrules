﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchedulleVNS.Algorithms.Interfaces;
using SchedulleVNS.Enums;

namespace SchedulleVNS.Algorithms
{
    class SptPriorityRuleAlgorithm: IPriorityRuleAlgorithm
    {
        public Double Coefficient { get; set; }

        public PriorityRuleType PriorityRuleType { get { return PriorityRuleType.SPT; } }

        public SptPriorityRuleAlgorithm()
        {
            Coefficient = 1;
        }

        public void Do(List<Machine> machines, int machineId, int currentTime)
        {
            var machine = machines.Find(item => item.Id == machineId);

            if (machine.WaitingForProcessingOperations.Count == 0)
            {
                return;
            }
            var maxProcessingTime = machine.WaitingForProcessingOperations.Max(item => item.ProcessingTime);
            var minProcessingTime = machine.WaitingForProcessingOperations.Min(item => item.ProcessingTime);

            if (maxProcessingTime == minProcessingTime)
            {
                foreach (var operation in machine.WaitingForProcessingOperations)
                {
                    operation.AddPriority(PriorityRuleType, 1);
                }
            }
            else
            {
                var priorityStep = (double)1 / (maxProcessingTime - minProcessingTime);

                foreach (var operation in machine.WaitingForProcessingOperations)
                {
                    var priority = (maxProcessingTime - operation.ProcessingTime) * priorityStep;

                    operation.AddPriority(PriorityRuleType, priority);
                }
            }
        }
    }
}
