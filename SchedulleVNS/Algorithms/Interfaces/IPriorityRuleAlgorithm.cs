﻿using System;
using System.Collections.Generic;
using SchedulleVNS.Enums;

namespace SchedulleVNS.Algorithms.Interfaces
{
    public interface IPriorityRuleAlgorithm
    {
        Double Coefficient { get; set; }

        PriorityRuleType PriorityRuleType { get;}

        void Do(List<Machine> machines, Int32 machineId, Int32 currentTime);
    }
}
