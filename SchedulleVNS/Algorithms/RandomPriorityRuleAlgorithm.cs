﻿using System;
using System.Collections.Generic;
using SchedulleVNS.Algorithms.Interfaces;
using SchedulleVNS.Enums;

namespace SchedulleVNS.Algorithms
{
    public class RandomPriorityRuleAlgorithm : IPriorityRuleAlgorithm
    {
        public Double Coefficient { get; set; }

        public PriorityRuleType PriorityRuleType { get { return PriorityRuleType.Random; } }

        public void Do(List<Machine> machines, int machineId, int currentTime)
        {
            var machine = machines.Find(item => item.Id == machineId);

            if (machine.WaitingForProcessingOperations.Count == 0)
            {
                return;
            }

            var random = new Random();
            foreach (var operation in machine.WaitingForProcessingOperations)
            {
                operation.AddPriority(PriorityRuleType, random.NextDouble());
            }

        }
    }
}
