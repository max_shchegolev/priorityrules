﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchedulleVNS.Algorithms.Interfaces;
using SchedulleVNS.Enums;

namespace SchedulleVNS.Algorithms
{
    public class CovertPriorityRuleAlgorithm : IPriorityRuleAlgorithm
    {
        public Double Coefficient { get; set; }

        public PriorityRuleType PriorityRuleType { get { return PriorityRuleType.COVERT; } }

        public double K { get; set; }

        public CovertPriorityRuleAlgorithm()
        {
            Coefficient = 1;
            K = 2;
        }

        public void Do(List<Machine> machines, Int32 machineId, Int32 currentTime)
        {
            var machine = machines.First(item => item.Id == machineId);

            if (machine.WaitingForProcessingOperations.Count == 0)
            {
                return;
            }

            //считаем приоритет 
            foreach (var operation in machine.WaitingForProcessingOperations)
            {
                //wspt is Weighted Shortest Processing Time
                var wspt = (operation.Job.weight / operation.ProcessingTime);

                //TODO check 0
                var totalTimeOfAllWaitingOperations =
                    machine.WaitingForProcessingOperations.Sum(item => item.ProcessingTime);
                //отнимается только время одной операции, возможно нужно отнимать время всех операция для данной работы?
                //тут явно какая-то хрень
                //FIXED
                var remainingProcessingTimeForJob =
                    operation.Job.operations.Where(item => item.OperationNumber > operation.OperationNumber)
                        .Sum(item => item.ProcessingTime);
                var priority = wspt *
                                Math.Max((1 - (Math.Max(operation.Job.dueDate - currentTime - remainingProcessingTimeForJob, 0)) / K * totalTimeOfAllWaitingOperations), 0);

                operation.AddPriority(PriorityRuleType, priority);
            }

            //делаем приоритет от 0 до 1
            var maxPriority = machine.WaitingForProcessingOperations.Max(item => item.GetPriority(PriorityRuleType));
            var minPriority = machine.WaitingForProcessingOperations.Min(item => item.GetPriority(PriorityRuleType));

            if (Math.Abs(maxPriority - minPriority) < 0.0001)
            {
                foreach (var operation in machine.WaitingForProcessingOperations)
                {
                    operation.AddPriority(PriorityRuleType, 1);
                }
            }
            else
            {
                var priorityStep = (double)1 / (maxPriority - minPriority);

                foreach (var operation in machine.WaitingForProcessingOperations)
                {
                    var priority = (operation.GetPriority(PriorityRuleType) - minPriority) * priorityStep;

                    operation.AddPriority(PriorityRuleType, priority);
                }
            }
        }
    }
}
