﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchedulleVNS.Algorithms.Interfaces;
using SchedulleVNS.Enums;

namespace SchedulleVNS.Algorithms
{
    public class CriticalRationPriorityRuleAlgorithm :IPriorityRuleAlgorithm
    {
        public double Coefficient { get; set; }

        public PriorityRuleType PriorityRuleType { get{return PriorityRuleType.CriticalRatio;} }

        public void Do(List<Machine> machines, int machineId, int currentTime)
        {
            var machine = machines.First(item => item.Id == machineId);

            if (machine.WaitingForProcessingOperations.Count == 0)
            {
                return;
            }

            //считаем приоритет 
            foreach (var operation in machine.WaitingForProcessingOperations)
            {

                var remainingProcessingTimeForJob =
                    operation.Job.operations.Where(item => item.OperationNumber > operation.OperationNumber)
                        .Sum(item => item.ProcessingTime);
                var priority = remainingProcessingTimeForJob == 0?0: (double)(operation.Job.dueDate - currentTime)/remainingProcessingTimeForJob;
                operation.AddPriority(PriorityRuleType, priority);
            }

            //ПОЧЕМУ ДЛЯ TEST JOB1 В ЕБЕНЯХ
            //делаем приоритет от 0 до 1
            var max= machine.WaitingForProcessingOperations.Max(item => item.GetPriority(PriorityRuleType));
            var min = machine.WaitingForProcessingOperations.Min(item => item.GetPriority(PriorityRuleType));

            if (Math.Abs(max - min) < 0.0001)
            {
                foreach (var operation in machine.WaitingForProcessingOperations)
                {
                    operation.AddPriority(PriorityRuleType, 1);
                }
            }
            else
            {
                var priorityStep = (double)1 / (max - min);

                foreach (var operation in machine.WaitingForProcessingOperations)
                {
                    var priority = (max - operation.GetPriority(PriorityRuleType)) * priorityStep;

                    operation.AddPriority(PriorityRuleType, priority);
                }
            }
        }
    }
}
