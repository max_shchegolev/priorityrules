﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchedulleVNS.Algorithms.Interfaces;
using SchedulleVNS.Enums;

namespace SchedulleVNS.Algorithms
{
    public class NinqPriorityRuleAlgorithm : IPriorityRuleAlgorithm
    {
        public double Coefficient { get; set; }

        public PriorityRuleType PriorityRuleType
        {
            get { return PriorityRuleType.Ninq; }
        }

        public void Do(List<Machine> machines, int machineId, int currentTime)
        {
            var machine = machines.Find(item => item.Id == machineId);

            if (machine.WaitingForProcessingOperations.Count == 0)
            {
                return;
            }

            foreach (var operation in machine.WaitingForProcessingOperations)
            {
                var nextOperation =
                    operation.Job.operations.FirstOrDefault(
                        item => item.OperationNumber == operation.OperationNumber + 1);
                if (nextOperation == null)
                {
                    operation.AddPriority(PriorityRuleType, 0);
                    continue;
                }
                //если следующей операции нет, то выставляем наивысший приоритет, чтобы быстрее закончить работу

                //теперь надо найти машину для этой операции 
                //потом найти releaseTime для текущей операции, сравнить с releaseTime для следующей операции и когда машина освободится,
                //посмотреть сколько операций в очередиы

                var machineForNextOperation = machines.First(item => item.FutureOperations.Contains(nextOperation));

                var currentOperationReleaseTime = currentTime + operation.ProcessingTime;

                //считаем что на тот момент когда операция будет выполнена - машина будет свободна

                var machineTime = currentOperationReleaseTime - currentTime;

                int remainingWaitingForProcessingOperationsCount = 0;
                //ебанутая логика
                if (machineForNextOperation.WaitingForProcessingOperations.Count != 0)
                {
                    //считаем сколько операций успеет выполниться
                    var averageProcessingTime =
                        machineForNextOperation.WaitingForProcessingOperations.Average(item => item.ProcessingTime);
                    var numOfPossibleProcessedOperations = (int) Math.Floor(machineTime/averageProcessingTime);
                    if (numOfPossibleProcessedOperations >= machineForNextOperation.WaitingForProcessingOperations.Count)
                    {
                        machineTime = Math.Max(machineTime -
                                               machineForNextOperation.WaitingForProcessingOperations.Sum(
                                                   item => item.ProcessingTime), 0);
                        remainingWaitingForProcessingOperationsCount = 0;
                    }
                    else
                    {
                        machineTime = 0;
                        remainingWaitingForProcessingOperationsCount =
                            Math.Max(
                                machineForNextOperation.WaitingForProcessingOperations.Count -
                                numOfPossibleProcessedOperations, 0);
                    }
                }


                //getminimumtime не работает
                var futureOperationForQueue = machineForNextOperation.FutureOperations.Where(
                    item => currentTime + item.GetMinimumTimeFromCurrentOperation <= currentOperationReleaseTime).ToList();

                

                int remainingFutureOperations = 0;

                if (machineTime > 0 && futureOperationForQueue.Count > 0)
                {
                    var futureOperationsAverageTime = futureOperationForQueue.Average(item => item.ProcessingTime);
                    var numOfPossibleProcessedOperations = (int) Math.Floor(machineTime/futureOperationsAverageTime);
                    if (numOfPossibleProcessedOperations >= futureOperationForQueue.Count)
                    {
                        machineTime = Math.Max(machineTime -
                                               futureOperationForQueue.Sum(
                                                   item => item.ProcessingTime), 0);
                        remainingFutureOperations = 0;
                    }
                    else
                    {
                        machineTime = 0;
                        remainingFutureOperations =
                            Math.Max(
                                futureOperationForQueue.Count -
                                numOfPossibleProcessedOperations, 0);
                    }
                }
                else
                {
                    remainingFutureOperations = futureOperationForQueue.Count;
                }

                var numOfFutureOperationsForQueue = remainingWaitingForProcessingOperationsCount +
                                                    remainingFutureOperations;

                //временно сохраняем в приоритет значение
                operation.AddPriority(PriorityRuleType, numOfFutureOperationsForQueue);

            }

            var maxOperationNumber = (int)machine.WaitingForProcessingOperations.Max(item => item.GetPriority(PriorityRuleType));
            var minOperationNumber =
                (int)machine.WaitingForProcessingOperations.Min(item => item.GetPriority(PriorityRuleType));

            if (maxOperationNumber == minOperationNumber)
            {
                foreach (var operation in machine.WaitingForProcessingOperations)
                {
                    operation.AddPriority(PriorityRuleType, 1);
                }
            }
            else
            {
                var priorityStep = (double)1 / (maxOperationNumber - minOperationNumber);

                foreach (var operation in machine.WaitingForProcessingOperations)
                {
                    var priority = (maxOperationNumber - operation.GetPriority(PriorityRuleType)) * priorityStep;

                    operation.AddPriority(PriorityRuleType, priority);
                }
            }
        }
    }
}
