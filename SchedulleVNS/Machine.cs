﻿using System;
using System.Collections.Generic;

namespace SchedulleVNS
{
    public class Machine
    {
        public Int32 ReleaseTime {get { return CurrentOperation != null ? CurrentOperation.ReleaseTime : 0; }}

        public Int32 Id { get; private set; }

        public List<Operation> ProcessedOperations { get; set; } 

        public List<Operation> WaitingForProcessingOperations { get; set; }

        public List<Operation> FutureOperations { get; set; }

        public Operation CurrentOperation { get; set; }

        public Boolean IsFree { get { return CurrentOperation == null; }}

        public Boolean AllDone { get { return FutureOperations.Count == 0 && WaitingForProcessingOperations.Count == 0 && CurrentOperation == null; } }

        public Machine(Int32 machineId)
        {
            Id = machineId;
            ProcessedOperations = new List<Operation>();
            WaitingForProcessingOperations = new List<Operation>();
            FutureOperations = new List<Operation>();
        }

        public void MoveOperationFromFutureToWaiting(Operation operation)
        {
            FutureOperations.Remove(operation);
            WaitingForProcessingOperations.Add(operation);
        }

        public void MoveOperationFromWaitingToCurrent(Operation operation, Int32 currentTime)
        {
            operation.s = currentTime;
            operation.ReleaseTime = currentTime + operation.ProcessingTime;
            operation.IsCurrent = true;
            WaitingForProcessingOperations.Remove(operation);
            CurrentOperation = operation;
        }

        public void MoveOperationFromCurrentToProcessed()
        {
            CurrentOperation.f = CurrentOperation.ReleaseTime;
            CurrentOperation.IsCurrent = false;
            ProcessedOperations.Add(CurrentOperation);
            CurrentOperation = null;
        }
        //public void MoveOperationFromFutureToWaiting(Operation operation)
        //{
        //    FutureOperations.Remove(operation);
        //    WaitingForProcessingOperations.Add(operation);
        //}

        //public void AddOperationToFutureOperations(Operation operation)
        //{
        //    if (!FutureOperations.Any(item => item.JobId == operation.JobId && item.OperationNumber == operation.OperationNumber))
        //    {
        //        FutureOperations.Add(operation);
        //    }
        //}
    }
}
