﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchedulleVNS.Enums;

namespace SchedulleVNS.Comparers
{
    public class OperationComparer : IComparer<Operation>
    {
        public Dictionary<PriorityRuleType, Double> PriorityRulesCoefficients { get; set; }

        public OperationComparer()
        {
            PriorityRulesCoefficients = new Dictionary<PriorityRuleType, Double>();
        }

        public void AddPriorityCoefficients(PriorityRuleType priorityRuleType, Double coefficient)
        {
            if (PriorityRulesCoefficients.ContainsKey(priorityRuleType))
            {
                PriorityRulesCoefficients[priorityRuleType] = coefficient;
            }
            else
            {
                PriorityRulesCoefficients.Add(priorityRuleType, coefficient);
            }
        }

        public int Compare(Operation leftOperation, Operation rightOperation)
        {
            var leftOperationPriority =
                PriorityRulesCoefficients.Sum(item => leftOperation.GetPriority(item.Key) * item.Value);

            var rightOperationPriority =
                PriorityRulesCoefficients.Sum(item => rightOperation.GetPriority(item.Key) * item.Value);

            return rightOperationPriority.CompareTo(leftOperationPriority);
        }
    }
}
