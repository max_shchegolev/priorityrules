﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchedulleVNS.Enums;

namespace SchedulleVNS
{
    class SchedulleManager
    {
        #region Private Fields

        private readonly int _totalMachineCount;

        private readonly int _maxIterations;

        private int _bestOfTillNow;

        private int _bestIterationTillNow;

        #endregion region

        /// <summary>
        /// All jobs
        /// </summary>
        List<Job> Jobs { get; set; }

        List<Job> BestSchedule { get; set; }
        Form1 father;
        public Stopwatch stopWatch;

        private BackgroundWorker bw = new BackgroundWorker();

        ProgressForm progressForm;
        
        public int CurrentIteration { get; set; }
        public bool isDone = false;
        public int currentOF = 0;

        public SchedulleManager(List<Job> jobs, int totalMachineCount, Form1 form)
        {
            BestSchedule = new List<Job>();
            Jobs = jobs;
            _totalMachineCount = totalMachineCount;
            _maxIterations = 10000;

            father = form;

            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = false;
            bw.DoWork += bw_DoWork;
            bw.ProgressChanged += bw_ProgressChanged;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            
            progressForm = new ProgressForm(_maxIterations);
            progressForm.Show(father);

            bw.RunWorkerAsync();
        }

        /// <summary>
        /// Generates the schedule.
        /// </summary>
        /// <param name="worker">The worker.</param>
        private void GenerateSchedule(BackgroundWorker worker)
        {
            stopWatch = new Stopwatch();
            var bestTardiness = Int32.MaxValue;
            for (int i = 0; i < 1000; ++i)
            {
                stopWatch.Start();

                var generator = new Generator(Jobs, _totalMachineCount);

                //Можно склеивать сколько угодно алгоритмов с любыми коэффициентами
                generator.GetSolution(new List<SelectedAlgorithmModel>
                {
                    new SelectedAlgorithmModel{Coefficient = 1, PriorityRuleType = PriorityRuleType.Random},
                   // new SelectedAlgorithmModel{Coefficient = 3, PriorityRuleType = PriorityRuleType.CriticalRatio},
                    //new SelectedAlgorithmModel{Coefficient = 2, PriorityRuleType = PriorityRuleType.SPT},
                });

                var tardiness = Generator.CalculateTardiness(Jobs);
                if (tardiness < bestTardiness)
                {
                    BestSchedule.Clear();
                    foreach (var job in Jobs)
                    {
                        BestSchedule.Add(job.Clone());
                    }
                    bestTardiness = tardiness;
                }
            }

            Jobs.Clear();
            foreach (var job in BestSchedule)
            {
                Jobs.Add(job);
            }
            stopWatch.Stop();
            isDone = true;
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            GenerateSchedule(worker);
        }
        
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!(e.Error == null))
            {
                
            }

            else
            {
                progressForm.Dispose();
                father.prepareSchedulle();
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressForm.update(CurrentIteration, _bestOfTillNow, currentOF, _bestIterationTillNow);
        }
    }
}
