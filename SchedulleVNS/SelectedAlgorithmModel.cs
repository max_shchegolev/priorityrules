﻿using System;
using SchedulleVNS.Enums;

namespace SchedulleVNS
{
    public class SelectedAlgorithmModel
    {
        public PriorityRuleType PriorityRuleType { get; set; }

        public Double Coefficient { get; set; }
    }
}
