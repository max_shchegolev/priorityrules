﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchedulleVNS.Algorithms;
using SchedulleVNS.Algorithms.Interfaces;
using SchedulleVNS.Comparers;
using SchedulleVNS.Enums;

namespace SchedulleVNS
{
    public class Generator
    {
        #region Private Properties

        private List<Machine> Machines { get; set; }

        #endregion

        #region Constructors

        public Generator(List<Job> jobs, Int32 machinesCount)
        {
            //создаем список машин
            Machines = new List<Machine>();
            for (int machineId = 1; machineId <= machinesCount; ++machineId)
            {
                Machines.Add(new Machine(machineId));
            }

            foreach (var job in jobs)
            {
                //делаем первую операцию каждой работы текущей
                job.StartExecuting();

                //заполняем очередь будущих работ для каждой машины
                foreach (var operation in job.operations)
                {
                    var machine = Machines.First(item => item.Id == operation.Machine);
                    machine.FutureOperations.Add(operation);
                }
            }
        }

        #endregion

        #region Public Methods 

        //  Для машины есть следующие виды операций: Операции который когда либо будут выполнены(Future), операции которые ожидают выполнения(Waiting),
        //операция которая выполняется(Current) и операции которые были выполнены(Processed). Эти множества не пересекаются. 
        //1.Для каждой машины, мы проверяем выполнилась ли текущая операция и если да, то переводим ее в соответсвующий список. 
        //  У каждой работы есть список операций, у операции есть свойство IsCurrent - т.е эта операция ожидает выполнения или выполняется.
        //  Поэтому если какая-то операция была выполнена, то устанавливаем слежующей операции IsCurrent = true
        //2.Для каждой машины переводим операции у которых IsCurrent = true из списка Future в список Waiting. //TODO проверить что бы работа не начала выполняться раньше положенного срока
        //3.Для каждой машины берем операции из Waiting и считаем для них их приоритетность, на 

        public void GetSolution(List<SelectedAlgorithmModel> algorithmsModels)
        {
            if (algorithmsModels == null || algorithmsModels.Count == 0)
            {
                throw new ArgumentException();
            }
            var algorithms = GetAlgorithms(algorithmsModels);

            var currentTime = 0;

            //remove at the end
            var tmpCounter = 0;
            //время - дискретная величина, двигаемся по временному интервалу
            //условие выхода из цикла - ни для одной машины не найдено невыполненных операций
            do
            {
                //Переносим в список выполненных работ те операции, которые только что закончились. Таким образом машины в этот момент освобождаются.
                CloseReleasedOperations(currentTime);

                //У каждой работы есть список операций. операции выполняются по очереди. Текущей операцией считается та, которая выполняется, или та, которая ожидает выполнения.
                //Проверяем для каждой машины список будущих операций и если есть текущие операции для каких-то работ - переносим их в список ожидания выполнения
                UpdateWaitingOperationList(currentTime);

                //Для каждой машины есть список операций которые ожидают выполнения. Если машина освободилась. то применяя правила приоритета выбераем ту операцию,
                // которая начнет выполняться в данный момент времени
                SelectOperationsToExecute(algorithms, currentTime);

                ++currentTime;
                ++tmpCounter;
            } while (!Machines.TrueForAll(item => item.AllDone) && tmpCounter < 10000);
        }

        //Когда расписание составлено считает общее опоздание
        //Вообще такую штуку использовать так, как она использовалась арабом не сильно круто, 
        //т.к она не учитывает вес работ
        public static Int32 CalculateTardiness(List<Job> jobs)
        {
            return jobs.Sum(item => Math.Max(item.operations.Max(operation => operation.f) - item.dueDate, 0));
        }
        #endregion

        #region Private Methods

        private void CloseReleasedOperations(Int32 currentTime)
        {
            foreach (var machine in Machines)
            {
                if (machine.CurrentOperation != null && machine.ReleaseTime == currentTime)
                {
                    var nextOperationForJob =
                        machine.CurrentOperation.Job.operations.FirstOrDefault(
                            item => item.OperationNumber == machine.CurrentOperation.OperationNumber + 1);
                    if (nextOperationForJob != null)
                    {
                        nextOperationForJob.IsCurrent = true;
                    }

                    machine.MoveOperationFromCurrentToProcessed();
                }
            }
        }

        private void UpdateWaitingOperationList(Int32 currentTime)
        {
            foreach (var machine in Machines)
            {
                var operationsToMove = machine.FutureOperations.Where(operation => operation.IsCurrent && operation.Job.releaseDate <= currentTime).ToList();
                foreach (var operation in operationsToMove)
                {
                    machine.MoveOperationFromFutureToWaiting(operation);
                }
            }
        }

        private void SelectOperationsToExecute(List<IPriorityRuleAlgorithm> algorithms, Int32 currentTime)
        {
            foreach (var machine in Machines)
            {
                if (!machine.IsFree)
                {
                    continue;
                }

                //объект который будет сравнивать две операции на основании уже выставленных коэффициентов приоритета
                //с помощью этого объекта функция sort может отсортировать список
                var comparer = new OperationComparer();

                // может использоваться сразу несколько алгоритмов, каждый алгоритм высчитывает свои приоритеты
                foreach (var algorithm in algorithms)
                {
                    //для каждой операции алгоритм высчитывает свой приоритет и присваивает его по ключу. Приоритет от 0 до 1
                    algorithm.Do(Machines, machine.Id, currentTime);
                    //говорим объекту с каким коэффициентом использовать приоритет от данного правила
                    comparer.AddPriorityCoefficients(algorithm.PriorityRuleType, algorithm.Coefficient);
                }

                //Например для какой-то операции выставлены следующие приоритеты:
                //  Random 0.6
                //  COVERT 0.7
                //  SPT    0.4
                //При этом коэффициенты для правил/алгоритмов следующие
                //  Random 0.5
                //  COVERT 1
                //  SPT    1
                //тогда итоговый приоритет операции при сравнении: 0.5 * 0.6 + 1 * 0.7 + 1 * 0.4 = 1.4

                if (machine.WaitingForProcessingOperations.Count != 0)
                {
                    //Сортируем используя специальный объект
                    machine.WaitingForProcessingOperations.Sort(comparer);
                    var operationToProcess = machine.WaitingForProcessingOperations.First();
                    //Начинаем выполнять операцию
                    machine.MoveOperationFromWaitingToCurrent(operationToProcess, currentTime);
                }
            }
        }

        private List<IPriorityRuleAlgorithm> GetAlgorithms(List<SelectedAlgorithmModel> algorithmsModels)
        {
            var algorithms = new List<IPriorityRuleAlgorithm>();

            foreach (var algorithmModel in algorithmsModels)
            {
                IPriorityRuleAlgorithm algorithm = null;

                if (algorithmModel.PriorityRuleType == PriorityRuleType.COVERT)
                {
                    algorithm = new CovertPriorityRuleAlgorithm();
                }
                else if (algorithmModel.PriorityRuleType == PriorityRuleType.SPT)
                {
                    algorithm = new SptPriorityRuleAlgorithm();
                }
                else if (algorithmModel.PriorityRuleType == PriorityRuleType.Random)
                {
                    algorithm = new RandomPriorityRuleAlgorithm();
                }
                else if (algorithmModel.PriorityRuleType == PriorityRuleType.Ninq)
                {
                    algorithm = new NinqPriorityRuleAlgorithm();
                }
                else if (algorithmModel.PriorityRuleType == PriorityRuleType.CriticalRatio)
                {
                    algorithm = new CriticalRationPriorityRuleAlgorithm();
                }

                if (algorithm != null)
                {
                    algorithm.Coefficient = algorithmModel.Coefficient;
                }

                algorithms.Add(algorithm);
            }

            return algorithms;
        }

        #endregion
    }


}
