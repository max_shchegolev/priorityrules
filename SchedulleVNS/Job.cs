﻿using System.Drawing;
using System.Linq;

namespace SchedulleVNS
{
    public class Job
    {
        public Operation[] operations;
        public Color color;
        public int id;
        public double weight;
        public int releaseDate;
        public int dueDate;
        public int tardiness;
        public Operation CurrentOperation { get; set; }

        public Job(int num, Color color)
        {
            operations = new Operation[num];
            this.color = color;
        }

        public Job(Job job)
        {
            operations = (Operation[])job.operations.Clone();
            color = job.color;
            id = job.id;
            weight = job.weight;
            releaseDate = job.releaseDate;
            dueDate = job.dueDate;
            tardiness = job.tardiness;
        }

        public void StartExecuting()
        {
            foreach (var operation in operations)
            {
                operation.IsCurrent = false;
            }
            operations.First().IsCurrent = true;
        }

        public Job Clone()
        {
            var jobClone = new Job(operations.Length, color);
            jobClone.id = id;
            jobClone.weight = weight;
            jobClone.releaseDate = releaseDate;
            jobClone.dueDate = dueDate;
            jobClone.tardiness = tardiness;

            for (int i = 0; i < operations.Length; ++i)
            {
                jobClone.operations[i] = operations[i].Clone(jobClone);
            }

            if (CurrentOperation != null)
            {
                jobClone.CurrentOperation =
                    jobClone.operations.First(item => item.OperationNumber == CurrentOperation.OperationNumber);
            }

            return jobClone;
        }
    }
}
