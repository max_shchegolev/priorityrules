﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulleVNS.Enums
{
    public enum PriorityRuleType
    {
        None = 0,

        Random = 1,
        //the shortest processing-time
        SPT = 2,

        //
//        Slack = 2,

        //the fewest number of operations remaining (8)
        COVERT = 3,

        Ninq = 4,

        CriticalRatio = 5
    }
}
