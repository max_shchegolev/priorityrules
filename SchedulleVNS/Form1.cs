﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchedulleVNS
{
    public partial class Form1 : Form
    {
        #region Fields

        List<ScheduleMachine> scheduleMachines;

        /// <summary>
        /// All jobs
        /// </summary>
        List<Job> Jobs { get; set; }

        /// <summary>
        /// Set of all machine numbers
        /// </summary>
        SortedSet<int> Machines;

        SchedulleManager scheduleManager;

        public bool isDone { get; set; }

        public Int32 leastTardiness { get; set; }

        #endregion

        #region Constructors

        public Form1()
        {
            InitializeComponent();
            
            Jobs = new List<Job>();
            scheduleMachines = new List<ScheduleMachine>();
            Machines = new SortedSet<int>();
        }

        #endregion

        private void ReadInput(string fileName)
        {
            string[] lines = File.ReadAllLines(fileName);

            Random rand = new Random();

            for (int i = 1; i < lines.Length; i++)
            {
                //get job id; ex. j1
                string s = lines[i].Substring(1, lines[i].IndexOf(':') - 1);
                int jobID = 0;
                int numberOfOperations = 0;
                int.TryParse(s, out jobID);

                //get the job parameters;  ex. 0,9,1,4
                s = lines[i].Substring(lines[i].IndexOf(':') + 2);
                s = s.Substring(0, s.IndexOf(':'));
                float[] data = s.Split(',').Select(p => float.Parse(p)).ToArray();
                numberOfOperations = (int)data[3];
                var job = new Job(numberOfOperations, Color.FromArgb(rand.Next(0, 255), rand.Next(0, 255), rand.Next(0, 255)));
                job.id = jobID;
                job.releaseDate = (int)data[0];
                job.dueDate = (int)data[1];
                job.weight = (double)data[2];

                //get operations of the job;  ex. (6_2),(4_3),(3_2),(1_2)
                s = lines[i].Substring(lines[i].IndexOf(':') + 2);
                s = s.Substring(s.IndexOf(':') + 1);
                string[] operationsArray = s.Split(',');
                for (int operationNumber = 0; operationNumber < numberOfOperations; operationNumber++)
                {
                    operationsArray[operationNumber] = operationsArray[operationNumber].Replace("(", "");
                    operationsArray[operationNumber] = operationsArray[operationNumber].Replace(")", "");

                    int machine = int.Parse(operationsArray[operationNumber].Substring(0, operationsArray[operationNumber].IndexOf('_')));
                    int processingTime = int.Parse(operationsArray[operationNumber].Substring(operationsArray[operationNumber].IndexOf('_') + 1));

                    if (!Machines.Contains(machine))
                    {
                        Machines.Add(machine);
                    }

                    var operation = new Operation(job.id, operationNumber + 1, machine, processingTime, job);
                    job.operations[operationNumber] = operation;
                }

                Jobs.Add(job);
            }
        }

        private void generateSchedule()
        {

            leastTardiness=Generator.CalculateTardiness(Jobs);
            scheduleMachines = new List<ScheduleMachine>();
            foreach (var machine in Machines)
            {
                var scheduleMachine = new ScheduleMachine();
                scheduleMachine.id = machine;
                scheduleMachines.Add(scheduleMachine);
            }
        }

        public void prepareSchedulle()
        {
            
            generateSchedule();
            int x = schedullePanel.Bounds.X + 20;
            int y = schedullePanel.Bounds.Y + 20;
            int height = schedullePanel.Bounds.Height - 80;
            int num = scheduleMachines.Count;
            int xIncrease = height / num;
            for (int i = 1; i <= scheduleMachines.Count; i++)
            {
                ScheduleMachine s = getScheduleMachine(i);
                s.x = x;
                s.y = y + ((i - 1) * xIncrease);
            }
            isDone = true;
            schedullePanel.Invalidate();
        }

        private ScheduleMachine getScheduleMachine(int id)
        {
            return scheduleMachines.FirstOrDefault(m => m.id == id);
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string filename = "";

            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Instance Files|*.TIC;*.TXT|All files (*.*)|*.*";
            DialogResult dialogResult = openFileDialog.ShowDialog();

            if (dialogResult == DialogResult.OK)
            {
                filename = openFileDialog.FileName;
            }

            if (filename != "")
            {
                Jobs = new List<Job>();
                Machines = new SortedSet<int>();
                ReadInput(filename);
                scheduleManager = new SchedulleManager(Jobs, Machines.Count, this);
                fixJobsColors();
                schedullePanel.Invalidate();
            }
        }

        private void Doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            float x = e.MarginBounds.Left;
            float y = e.MarginBounds.Top;
            Bitmap bmp = new Bitmap(this.schedullePanel.Width, this.schedullePanel.Height);
            this.schedullePanel.DrawToBitmap(bmp, new Rectangle(0, 0, this.schedullePanel.Width, this.schedullePanel.Height));
            e.Graphics.DrawImage((Image)bmp, x, y);
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Normal;
            this.Height = 436;
            this.Width = 630;
            schedullePanel.Invalidate();
            PrintDocument doc = new PrintDocument();
            doc.PrintPage += this.Doc_PrintPage;
            PrintDialog dlgSettings = new PrintDialog();
            dlgSettings.Document = doc;
            if (dlgSettings.ShowDialog() == DialogResult.OK)
            {
                doc.Print();
            }
        }

        private void schedulePanel_Paint(object sender, PaintEventArgs e)
        {
            if (Jobs.Count != 0 && isDone)
            {
                e.Graphics.Clear(Color.White);
                Pen p = new Pen(Color.Black);
                Font font = new System.Drawing.Font("Arial", 12);
                Font font2 = new System.Drawing.Font("Arial", 8);
                SizeF textSize = e.Graphics.MeasureString("8", this.Font);
                SolidBrush textBrush = new SolidBrush(Color.White);
                Font f = new System.Drawing.Font("Arial", 12);
                SolidBrush b = new SolidBrush(Color.Black);
                int shiftRight = 20;
                int x = schedullePanel.Bounds.X + shiftRight;
                int y = schedullePanel.Bounds.Y + shiftRight;
                int width = schedullePanel.Bounds.Width - x;
                int height = schedullePanel.Bounds.Height - y;
                int num = scheduleMachines.Count;
                int xIncrease = height / num;
                int rectangleHeight = 25;
                for (int i = 1; i <= scheduleMachines.Count; i++)
                {
                    ScheduleMachine s = getScheduleMachine(i);
                    s.x = x;
                    s.y = y + ((i - 1) * xIncrease);
                }
                int maxLength = 0;
                foreach (Job j in Jobs)
                {
                    if (j.operations[j.operations.Length - 1].f > maxLength)
                        maxLength = j.operations[j.operations.Length - 1].f;
                }
                int scale = (width - 20) / (maxLength);

                foreach (ScheduleMachine m in scheduleMachines)
                {
                    e.Graphics.DrawLine(p, m.x, m.y, width, m.y);
                    e.Graphics.DrawString("M" + m.id, font, b, m.x - 15, m.y);
                }

                int currentWidth = shiftRight, counter = 1;
                while (currentWidth <= width)
                {
                    currentWidth += scale;
                    ScheduleMachine s = getScheduleMachine(scheduleMachines.Count);
                    e.Graphics.DrawLine(p, currentWidth, s.y, currentWidth, s.y + 10);
                    int size = (int)e.Graphics.MeasureString(counter + "", this.Font).Width;
                    e.Graphics.DrawString("" + counter, font2, b, currentWidth - size, s.y + 12);
                    counter++;
                }

                foreach (Job j in Jobs)
                {
                    Pen pen = new Pen(j.color);
                    SolidBrush brush = new SolidBrush(j.color);
                    for (int i = 0; i < j.operations.Length; i++)
                    {
                        Operation o1 = j.operations[i];

                        ScheduleMachine s1 = getScheduleMachine(o1.Machine);

                        Rectangle rec = new Rectangle((o1.s) * scale + shiftRight, s1.y - rectangleHeight, ((o1.s + o1.ProcessingTime) * scale) - ((o1.s) * scale), rectangleHeight);
                        e.Graphics.FillRectangle(brush, rec);

                        if (o1.f > o1.s + o1.ProcessingTime)
                            e.Graphics.DrawRectangle(pen, (o1.s + o1.ProcessingTime) * scale + shiftRight, s1.y - rectangleHeight, (o1.f - (o1.s + o1.ProcessingTime)) * scale, rectangleHeight);
                        if (i < j.operations.Length - 1)
                        {
                            Operation o2 = j.operations[i + 1];
                            ScheduleMachine s2 = getScheduleMachine(o2.Machine);
                            e.Graphics.DrawLine(pen, (o1.f) * scale + shiftRight, s1.y, (o2.s) * scale + shiftRight, s2.y);
                        }
                        int triangleWidth = 20;
                        if (i == 0)
                        {
                            e.Graphics.FillPolygon(Brushes.Black, new Point[] { new Point((o1.s) * scale + shiftRight, s1.y - rectangleHeight), new Point((o1.s) * scale + shiftRight, s1.y), new Point((o1.s) * scale + shiftRight + triangleWidth, s1.y - rectangleHeight / 2) });
                        }
                        if (i == (j.operations.Length - 1))
                        {
                            e.Graphics.FillPolygon(Brushes.Black, new Point[] { new Point(((o1.s + o1.ProcessingTime) * scale) + shiftRight, s1.y - rectangleHeight), new Point(((o1.s + o1.ProcessingTime) * scale) + shiftRight, s1.y), new Point(((o1.s + o1.ProcessingTime) * scale) + shiftRight - triangleWidth, s1.y - rectangleHeight / 2) });
                        }
                        e.Graphics.DrawString("" + j.id, font, textBrush, new PointF(rec.X + (rec.Width / 2) - (textSize.Width / 2), rec.Y + (rec.Height / 2) - (textSize.Height / 2)));
                        e.Graphics.DrawString("" + j.id, font, textBrush, new PointF(rec.X + (rec.Width / 2) - (textSize.Width / 2), rec.Y + (rec.Height / 2) - (textSize.Height / 2)));
                        e.Graphics.DrawString("" + j.id, font, textBrush, new PointF(rec.X + (rec.Width / 2) - (textSize.Width / 2), rec.Y + (rec.Height / 2) - (textSize.Height / 2)));
                        e.Graphics.DrawString("" + j.id, font, textBrush, new PointF(rec.X + (rec.Width / 2) - (textSize.Width / 2), rec.Y + (rec.Height / 2) - (textSize.Height / 2)));
                    }

                }
                TimeSpan ts = scheduleManager.stopWatch.Elapsed;

                // Format and display the TimeSpan value. 
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds,
                    ts.Milliseconds / 10);
                
                //e.Graphics.DrawString("Objective Function: " + this.scheduleManager.bestSchedule.of + "     CPU:" + elapsedTime + "        iterations: " + scheduleManager.CurrentIteration, font, b, 40, schedullePanel.Bounds.Height - 30);
                e.Graphics.DrawString("Objective Function: " + 0+ "     CPU:" + elapsedTime + "        Total Tardiness: " + leastTardiness, font, b, 40, schedullePanel.Bounds.Height - 30);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog dialog = new SaveFileDialog())
            {
                dialog.Filter = "Image files (*.jpg)|*.jpg";
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    Bitmap bmp = new Bitmap(this.schedullePanel.Width, this.schedullePanel.Height);
                    this.schedullePanel.DrawToBitmap(bmp, new Rectangle(0, 0, this.schedullePanel.Width, this.schedullePanel.Height));
                    bmp.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }
        }

        private void fixJobsColors()
        {
            Type t = typeof(Color);
            var colorProperties = t.GetProperties();

            // Array of colors
            Color[] colors = colorProperties.Where(prop => prop.PropertyType == t)
                                        .Select(prop => (Color)prop.GetValue(null, null))
                                        .ToArray();
            int currentjob = 0;
            for (int i = 2; i < colors.Length; i++)
            {
                if (i == 3 || i == 5)
                    continue;
                if (!((colors[i].R < 120 && colors[i].G > 120 && colors[i].B > 120) || (colors[i].R > 120 && colors[i].G < 120 && colors[i].B > 120) || (colors[i].R > 120 && colors[i].G > 120 && colors[i].B < 120)))
                    continue;
                if (currentjob < Jobs.Count)
                {
                    Jobs[currentjob].color = colors[i];
                    currentjob++;
                }
                else
                    break;
            }
        }

        #region Overriden Methods

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                // Check your window state here
                if (m.WParam == new IntPtr(0xF030)) // Maximize event - SC_MAXIMIZE from Winuser.h
                {
                    // THe window is being maximized

                }
                schedullePanel.Invalidate();
            }
            base.WndProc(ref m);
        }

        #endregion
    }
}
