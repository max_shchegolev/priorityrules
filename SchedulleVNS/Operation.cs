﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchedulleVNS.Enums;

namespace SchedulleVNS
{
    public class Operation
    {
        public Job Job { get; set; }
        public int JobId { get { return Job.id; }  }

        public int OperationNumber { get; set; }

        public int Machine { get; set; }

        public int ProcessingTime { get; set; }

        public Boolean IsCurrent { get; set; }

        public Int32 StartTime { get; set; }

        public Int32 ReleaseTime { get; set; }

        private Dictionary<PriorityRuleType, double> Priorities { get; set; }

        public Int32 GetMinimumTimeFromCurrentOperation
        {
            get
            {
                var currentOperation = Job.operations.First(item => item.IsCurrent);

                return
                    Job.operations.Where(
                        item =>
                            item.OperationNumber > currentOperation.OperationNumber &&
                            item.OperationNumber < OperationNumber).Sum(item => item.ProcessingTime);
            }
        }

        /// <summary>
        /// Return operation execute start time
        /// </summary>
        public int s;

        /// <summary>
        /// Return operation execute end time
        /// </summary>
        public int f;

        public Operation(int jobId, int operationNumber, int machine, int processingTime, Job job)
        {
            Job = job;
            OperationNumber = operationNumber;
            Machine = machine;
            ProcessingTime = processingTime;
            Priorities = new Dictionary<PriorityRuleType, double>();
        }

        public override string ToString()
        {
            return "(" + JobId + "," + OperationNumber + "," + Machine + ")";
        }

        public void ClearPriorities()
        {
            Priorities.Clear();
        }

        public void AddPriority(PriorityRuleType priorityRuleType, double priority)
        {
            if (Priorities.ContainsKey(priorityRuleType))
            {
                Priorities[priorityRuleType] = priority;
            }
            else
            {
                Priorities.Add(priorityRuleType, priority);
            }
        }

        public Double GetPriority(PriorityRuleType priorityRuleType)
        {
            if (Priorities.ContainsKey(priorityRuleType))
            {
                return Priorities[priorityRuleType];
            }

            throw new ArgumentException();
        }

        public Operation Clone(Job job)
        {
            var result = new Operation(Job.id, OperationNumber, Machine, ProcessingTime, Job)
            {
                IsCurrent = IsCurrent,
                StartTime = StartTime,
                ReleaseTime = ReleaseTime,
                s = s,
                f = f
            };
            result.Job = job;

            foreach (var priority in Priorities)
            {
                result.AddPriority(priority.Key, priority.Value);
            }

            return result;
        }
    }
}
