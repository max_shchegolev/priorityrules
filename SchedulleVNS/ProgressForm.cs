﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchedulleVNS
{
    public partial class ProgressForm : Form
    {
        int max;
        public ProgressForm(int m)
        {
            InitializeComponent();
            max = m;
            label.Text = "Iteration: 0 / " + max;
            TotalIterationsDoneLabel.Text = "Total inner iterations done: 0";
            bestOfIteration.Text = "Best OF found in: 0 / " + max;
        }

        public void update(int current, int of, int currentOF, int bestIteration)
        {
            label.Text = "Iteration: " + current + " / " + max;
            bestOF.Text = "Best OF Found : " + of;
            TotalIterationsDoneLabel.Text = "Current OF: " + currentOF;
            bestOfIteration.Text = "Best OF found in: " + bestIteration + " / " + max;
        }

    }
}
