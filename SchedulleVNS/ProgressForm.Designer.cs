﻿namespace SchedulleVNS
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label = new System.Windows.Forms.Label();
            this.bestOF = new System.Windows.Forms.Label();
            this.TotalIterationsDoneLabel = new System.Windows.Forms.Label();
            this.bestOfIteration = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SchedulleVNS.Properties.Resources.loading8;
            this.pictureBox1.Location = new System.Drawing.Point(183, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(178, 145);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(180, 183);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(22, 17);
            this.label.TabIndex = 1;
            this.label.Text = "zz";
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bestOF
            // 
            this.bestOF.AutoSize = true;
            this.bestOF.Location = new System.Drawing.Point(180, 238);
            this.bestOF.Name = "bestOF";
            this.bestOF.Size = new System.Drawing.Size(22, 17);
            this.bestOF.TabIndex = 2;
            this.bestOF.Text = "ss";
            // 
            // TotalIterationsDoneLabel
            // 
            this.TotalIterationsDoneLabel.AutoSize = true;
            this.TotalIterationsDoneLabel.Location = new System.Drawing.Point(180, 200);
            this.TotalIterationsDoneLabel.Name = "TotalIterationsDoneLabel";
            this.TotalIterationsDoneLabel.Size = new System.Drawing.Size(46, 17);
            this.TotalIterationsDoneLabel.TabIndex = 3;
            this.TotalIterationsDoneLabel.Text = "label1";
            // 
            // bestOfIteration
            // 
            this.bestOfIteration.AutoSize = true;
            this.bestOfIteration.Location = new System.Drawing.Point(180, 221);
            this.bestOfIteration.Name = "bestOfIteration";
            this.bestOfIteration.Size = new System.Drawing.Size(46, 17);
            this.bestOfIteration.TabIndex = 3;
            this.bestOfIteration.Text = "label1";
            // 
            // ProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 292);
            this.ControlBox = false;
            this.Controls.Add(this.bestOfIteration);
            this.Controls.Add(this.TotalIterationsDoneLabel);
            this.Controls.Add(this.bestOF);
            this.Controls.Add(this.label);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ProgressForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Working";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label bestOF;
        private System.Windows.Forms.Label TotalIterationsDoneLabel;
        private System.Windows.Forms.Label bestOfIteration;
    }
}